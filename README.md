# OpenML dataset: Economic_Census_Delhi

https://www.openml.org/d/46096

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Description: The delhi_state.csv dataset is a structured collection of data associated with various economic, demographic, and social attributes of areas within Delhi state, India. This extensive dataset provides insight into different dimensions including but not limited to the administration (State, District, Tehsil), workforce composition (WC, EB, EBX, BACT, NIC3, TOTAL_WORKER), household characteristics (C_HOUSE, IN_HH, OWN_SHIP_C), personal demographics (SEX, SG, RELIGION), and other assorted metrics like T_V (Total Value), HLOOM_ACT (Handloom Activity), NOP (Number of Persons), SOF (Source of Finance), M_H (Male Headed Households), F_H (Female Headed Households), M_NH (Male Non-headed Households), F_NH (Female Non-headed Households), and the operational SECTOR of the workforce. Key identifiers like the DISTRICT code provide locational specificity enhancing the dataset's utility for regional analysis.

Attribute Description:
- State, District, Tehsil: Administrative identifiers.
- T_V: Numeric, represents a Total Value (could be economic or demographic in nature).
- WC, EB, BACT, NIC3, TOTAL_WORKER: Workforce composition metrics, numeric.
- EBX, HLOOM_ACT, IN_HH, C_HOUSE, OWN_SHIP_C: Economic and household characteristic indicators, binary or numeric.
- SEX, SG, RELIGION: Demographic attributes, coded numerically.
- NOP, SOF: Numeric, detailing household composition and financial sources.
- M_H, F_H, M_NH, F_NH: Household headship indicators, numeric.
- SECTOR, DISTRICT: Numeric codes reflecting the area of employment and locational identification.

Use Case:
This dataset is invaluable for policymakers, researchers, and NGOs focusing on urban planning, socioeconomic development, and demographic studies within Delhi. It can facilitate targeted interventions, policy formulation, and understand community-specific needs. Additionally, socio-economic researchers can analyze workforce dynamics, household structures, and demographic trends to derive meaningful insights into the social fabric of Delhi.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/46096) of an [OpenML dataset](https://www.openml.org/d/46096). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/46096/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/46096/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/46096/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

